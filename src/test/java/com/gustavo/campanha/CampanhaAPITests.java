package com.gustavo.campanha;

import com.gustavo.campanha.controller.APIController;
import com.gustavo.campanha.model.Campanha;
import com.gustavo.campanha.model.Time;
import com.gustavo.campanha.service.CampanhaService;
import com.gustavo.campanha.service.TimeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.web.context.WebApplicationContext;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CampanhaAPITests {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext context;

	@Autowired
	ObjectMapper objectMapper;

	@Mock
	CampanhaService campanhaService;

	@Mock
	TimeService timeService;

	@InjectMocks
	APIController apiController;

	@Before
	public void setUp(){
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}

	@Test
	public void getListCampanhas() throws Exception {
		this.mockMvc.perform(get("/api/campanha").accept(MediaType.APPLICATION_JSON_VALUE))
				.andDo(print())
				.andExpect(status().isOk());

	}

	@Test
	public void getCampanhaById() throws Exception {
		final Long id = 1l;
		this.mockMvc.perform(get(String.format("/api/campanha/%s", id)).accept(MediaType.APPLICATION_JSON_VALUE))
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void getCampanhaByIdNotFound() throws Exception {
		final Long id = 9l;
		this.mockMvc.perform(get(String.format("/api/campanha/%s", id)).accept(MediaType.APPLICATION_JSON_VALUE))
				.andDo(print())
				.andExpect(status().isNotFound());
	}

	@Test
	public void removeCampanha() throws Exception{
		final Long id = 2l;
		this.mockMvc.perform(delete(String.format("/api/campanha/%s",id))
				.accept(MediaType.APPLICATION_JSON_VALUE))
				.andDo(print())
				.andExpect(status().isNoContent());
	}

	@Test
	public void updateCampanha() throws Exception{
		final Logger logger = LoggerFactory.getLogger(APIController.class);
		final Long id = 1l;
		String campanhaJson = createCampanhaInJson("Campanha 3", "01/10/2017", "03/10/2017");
		logger.info("update {}", campanhaJson);
		this.mockMvc.perform(put(String.format("/api/campanha/%s",id))
				.content(campanhaJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void createCampanha() throws Exception {
		final Logger logger = LoggerFactory.getLogger(APIController.class);
		String campanhaJson = createCampanhaInJson("Campanha 3", "01/10/2017", "03/10/2017");
		logger.info("create {}", campanhaJson);
		this.mockMvc.perform(post("/api/campanha/")
				.content(campanhaJson)
				.contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print())
				.andExpect(status().isCreated());
	}

	private static String createCampanhaInJson (String name, String dateStart, String dateEnd) {
		return "{ \"name\": \"" + name + "\", " +
				"\"team\":1, "+
				"\"dateStart\":\"" + dateStart + "\"," +
				"\"dateEnd\":\"" + dateEnd + "\"}";
	}
}
