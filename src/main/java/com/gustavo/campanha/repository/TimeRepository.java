package com.gustavo.campanha.repository;

import com.gustavo.campanha.model.Time;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TimeRepository extends JpaRepository<Time, Long> {

}
