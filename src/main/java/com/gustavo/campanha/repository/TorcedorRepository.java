package com.gustavo.campanha.repository;

import com.gustavo.campanha.model.Torcedor;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TorcedorRepository extends JpaRepository<Torcedor, Long> {
    Torcedor findByEmail(String email);
}
