package com.gustavo.campanha.repository;

import com.gustavo.campanha.model.Campanha;
import com.gustavo.campanha.model.Time;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface CampanhaRepository extends JpaRepository<Campanha, Long> {
    @Query("SELECT c FROM Campanha c WHERE c.dateEnd >= :date ORDER BY c.id")
    List<Campanha> getCampanhasByVigencia(@Param("date") LocalDate date);

    @Query("SELECT c FROM Campanha c WHERE c.dateStart >= :dateStart AND c.dateEnd <= :dateEnd ORDER BY c.dateEnd ASC")
    List<Campanha> getCampanhasByVigenciaPeriodo(@Param("dateStart") LocalDate dateStart, @Param("dateEnd") LocalDate dateEnd);

    @Query("select c FROM Campanha c WHERE c.team = :time AND c.dateEnd >= :date")
    List<Campanha> getCampanhaByVigenciaTime(@Param("date")LocalDate date, @Param("time")Time time);
}
