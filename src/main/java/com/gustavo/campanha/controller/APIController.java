package com.gustavo.campanha.controller;

import com.gustavo.campanha.model.Campanha;
import com.gustavo.campanha.model.Time;
import com.gustavo.campanha.model.Torcedor;
import com.gustavo.campanha.service.CampanhaService;
import com.gustavo.campanha.service.TimeService;
import com.gustavo.campanha.service.TorcedorService;
import com.gustavo.campanha.util.CustomMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/campanha")
public class APIController {

    public static final Logger logger = LoggerFactory.getLogger(APIController.class);

    @Autowired
    CampanhaService campanhaService;
    @Autowired
    TimeService timeService;

    @Autowired
    TorcedorService torcedorService;

    // -------------------List Campanhas---------------------------------------------
    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<Campanha>> listAllCampanhas(){
        LocalDate today = LocalDate.now();
        List<Campanha> campanhas = campanhaService.findCampanhasVigencia(today);
        if(campanhas.isEmpty()){
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Campanha>>(campanhas, HttpStatus.OK);
    }

    // -------------------Get Campanha by Id---------------------------------------------
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> getCampanha(@PathVariable("id") long id){
        logger.info("Getting by Id {}", id);
        Campanha campanha = campanhaService.findById(id);
        if(campanha == null){
            logger.info("Campanha id {} not found", id);
            return new ResponseEntity(new CustomMessage("Campanha ID "+ id + "not found"), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Campanha>(campanha, HttpStatus.OK);
    }

    // -------------------Create Campanha---------------------------------------------
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json;charset=UTF-8")
    public ResponseEntity<?> createCampanha(@RequestBody @Valid Campanha campanha) throws IllegalArgumentException{
        logger.info("Creating campanha {}", campanha);

        campanhaService.saveCampanha(campanha);

        return new ResponseEntity<>(campanha, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/torcedor", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8")
    public ResponseEntity<?> registerClienteCampanha(@RequestBody @Valid Torcedor torcedor) throws IllegalArgumentException{
        logger.info("registering campanha to user");
        List<Campanha> campanhas = campanhaService.findCampanhasVigenciaTime(LocalDate.now(), torcedor.getTeam());
        torcedor.setCampanhas(campanhas);
        torcedorService.saveTorcedor(torcedor);

        //HttpHeaders httpHeaders = new HttpHeaders();
        //httpHeaders.setLocation(uriComponentsBuilder.path("/tim/{id}").buildAndExpand(torcedor.getTeam().getId()).toUri());
        //return new ResponseEntity<String>(httpHeaders, HttpStatus.CREATED);
        return new ResponseEntity<List>(campanhas, HttpStatus.CREATED);

    }

    // -------------------Update Campanha by Id---------------------------------------------
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, consumes = "application/json;charset=UTF-8")
    public ResponseEntity<?> updateCampanha(@PathVariable("id") long id, @RequestBody @Valid Campanha campanha) throws IllegalArgumentException{
        logger.info("Updating campanha {}", id);
        Campanha currentCampanha = campanhaService.findById(id);
        if(currentCampanha == null){
            logger.info("Campanha {} not found, update not possible", id);
            return new ResponseEntity(new CustomMessage("Campanha with ID " + id + " not found and not update possible"), HttpStatus.NOT_FOUND);
        }
        currentCampanha.setName(campanha.getName());
        currentCampanha.setTeam(campanha.getTeam());
        currentCampanha.setDateStart(campanha.getDateStart());
        currentCampanha.setDateEnd(campanha.getDateEnd());

        campanhaService.updateCampanha(currentCampanha);

        return new ResponseEntity<Campanha>(currentCampanha, HttpStatus.OK);
    }

    // -------------------Delete Campanha by Id---------------------------------------------
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<?> deleteCampanha(@PathVariable("id") long id){
        logger.info("Deleting campanha {}", id);
        Campanha campanha = campanhaService.findById(id);
        if(campanha == null){
            logger.info("Campanha {} not found", id);
            return new ResponseEntity(new CustomMessage("Campanha ID" + id + "not found and not possible to delete!"), HttpStatus.NOT_FOUND);
        }
        campanhaService.deleteCampanha(id);
        return new ResponseEntity<Campanha>(HttpStatus.NO_CONTENT);
    }

    // -------------------List Campanhas---------------------------------------------
    @RequestMapping(value = "/time/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<List<Campanha>> listCampanhaTime(@PathVariable("id") long id){
        LocalDate today = LocalDate.now();
        Time time = timeService.findById(id);
        if(time == null){
            logger.info("Time {} not foun", id);
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        List<Campanha> campanhas = campanhaService.findCampanhasVigenciaTime(today, time);
        if(campanhas.isEmpty()){
            logger.info("Campanhas Vigentes team {} is empty", time.getName());
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(campanhas, HttpStatus.OK);
    }
}
