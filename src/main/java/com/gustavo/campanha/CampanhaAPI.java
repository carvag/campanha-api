package com.gustavo.campanha;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CampanhaAPI {
	public static void main(String[] args) throws Exception {
		SpringApplication.run(CampanhaAPI.class, args);
	}
}
