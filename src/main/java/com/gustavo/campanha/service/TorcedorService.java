package com.gustavo.campanha.service;

import com.gustavo.campanha.model.Torcedor;

public interface TorcedorService {

    Torcedor saveTorcedor(Torcedor torcedor);

    void updateTorcedor(Torcedor torcedor);
}
