package com.gustavo.campanha.service;

import com.gustavo.campanha.model.Campanha;
import com.gustavo.campanha.model.Time;

import java.time.LocalDate;
import java.util.List;

public interface CampanhaService {

    Campanha findById(Long id);

    List<Campanha> findCampanhasVigenciaTime(LocalDate date, Time time);

    List<Campanha> findCampanhasVigencia(LocalDate date);

    Campanha saveCampanha(Campanha campanha);

    void updateCampanha(Campanha campanha);

    void deleteCampanha(Long id);
}
