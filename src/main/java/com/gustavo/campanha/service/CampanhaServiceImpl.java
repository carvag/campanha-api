package com.gustavo.campanha.service;

import com.gustavo.campanha.controller.APIController;
import com.gustavo.campanha.model.Campanha;
import com.gustavo.campanha.model.Time;
import com.gustavo.campanha.repository.CampanhaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service("campanhaService")
@Transactional
public class CampanhaServiceImpl implements CampanhaService{

    @Autowired
    private CampanhaRepository campanhaRepository;

    public static final Logger logger = LoggerFactory.getLogger(APIController.class);

    public Campanha findById(Long id) {
        return campanhaRepository.findOne(id);
    }

    public List<Campanha> findCampanhasVigencia(LocalDate date) {
        return campanhaRepository.getCampanhasByVigencia(date);
    }

    public List<Campanha> findCampanhasVigenciaPeriodo(LocalDate dateStart, LocalDate dateEnd){
        return campanhaRepository.getCampanhasByVigenciaPeriodo(dateStart, dateEnd);
    }

    public Campanha saveCampanha(Campanha campanha) {
        List<Campanha> campanhaList = this.findCampanhasVigenciaPeriodo(campanha.getDateStart(), campanha.getDateEnd());
        campanhaList.forEach(campanhaEach -> {
            campanhaEach.setDateEnd(campanhaEach.getDateEnd().plusDays(1));
            addDiaFimVigenciaRecursivo(campanhaEach, campanhaList);
        });
        campanhaRepository.save(campanhaList);
        return campanhaRepository.save(campanha);
    }

    private void addDiaFimVigenciaRecursivo(Campanha campanha, List<Campanha> campanhaList){
        if(campanhaList.stream().filter(campanhaEach -> !campanhaEach.equals(campanha))
                .anyMatch(campanhaEach -> campanhaEach.getDateEnd().isEqual(campanha.getDateEnd()))){

            campanha.setDateEnd(campanha.getDateEnd().plusDays(1));
            addDiaFimVigenciaRecursivo(campanha, campanhaList);
        }
    }

    public void updateCampanha(Campanha campanha) {
        this.saveCampanha(campanha);
    }

    public void deleteCampanha(Long id) {
        campanhaRepository.delete(id);
    }

    public List<Campanha> findCampanhasVigenciaTime(LocalDate date, Time time){
        return campanhaRepository.getCampanhaByVigenciaTime(date, time);
    }
}
