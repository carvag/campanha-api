package com.gustavo.campanha.service;

import com.gustavo.campanha.model.Campanha;
import com.gustavo.campanha.model.Time;
import com.gustavo.campanha.model.Torcedor;
import com.gustavo.campanha.repository.CampanhaRepository;
import com.gustavo.campanha.repository.TorcedorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service("torcedorService")
public class TorcedorServiceImpl implements TorcedorService {

    @Autowired
    TorcedorRepository torcedorRepository;

    @Autowired
    CampanhaRepository campanhaRepository;

    public Torcedor saveTorcedor(Torcedor torcedor) {
        if(torcedorRepository.findByEmail(torcedor.getEmail()) != null){
            Torcedor torcedor1 = torcedorRepository.findByEmail(torcedor.getEmail());
            List<Campanha> campanhaList = campanhaRepository.getCampanhaByVigenciaTime(LocalDate.now(), torcedor.getTeam());
            torcedor1.setCampanhas(campanhaList);
            return torcedor;
        }
        return torcedorRepository.save(torcedor);
    }

    public void updateTorcedor(Torcedor torcedor) {
        this.saveTorcedor(torcedor);

    }
}
