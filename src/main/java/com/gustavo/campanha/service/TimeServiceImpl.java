package com.gustavo.campanha.service;


import com.gustavo.campanha.model.Time;
import com.gustavo.campanha.repository.TimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service("timeService")
@Transactional
public class TimeServiceImpl implements TimeService{

    @Autowired
    private TimeRepository timeRepository;

    @Override
    public Time findById(Long id) {
        return timeRepository.findOne(id);
    }

    @Override
    public List<Time> findAllTimes() {
        return timeRepository.findAll();
    }
}
