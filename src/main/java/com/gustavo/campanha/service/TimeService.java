package com.gustavo.campanha.service;

import com.gustavo.campanha.model.Time;

import java.util.List;

public interface TimeService {

    Time findById(Long id);

    List<Time> findAllTimes();
}
