# API Campanha

Códigos desenvolvidos para prover serviços rest 

Instruções específicas:
## CampanhaAPI
Efetuar o build com o Maven (mvn clean package spring-boot:run) e após isso executr com PostMan, curl, etc.

## Banco H2
```sh
http://localhost:8080/console

```

## Endpoint
```sh
http://localhost:8080/api/campanha
```

#### Create

```sh
curl -H "Accept: application/json" -H "Content-Type: application/json"  -X POST -d '{"name":"Campanha 3","team":2, "dateStart":"01/10/2017", "dateEnd":"03/10/2017"}' http://localhost:8080/api/campanha/
```
#### Update

```sh
curl -H "Accept: application/json" -H "Content-Type: application/json"  -X PUT -d '{"name":"Campanha 3","team":2, "dateStart":"01/10/2017", "dateEnd":"04/10/2017"}' http://localhost:8080/api/campanha/
```

#### Get BY ID

```sh
curl -H "Accept: application/json" -H "Content-Type: application/json"  -X GET http://localhost:8080/api/campanha/id
```

#### Get todas campanhas vigentes

```sh
curl -H "Accept: application/json" -H "Content-Type: application/json"  -X GET http://localhost:8080/api/campanha/
```

#### Delete

```sh
curl -H "Accept: application/json" -H "Content-Type: application/json"  -X DELETE http://localhost:8080/api/campanha/id
```
####Times
```sh
1 vasco
2 Flamengo
3 Fluminense
4 Botafogo
```

## Tecnologias Usadas

 - Java versão 8.
 - Maven: ferramenta de automação de compilação
 - JPA / Hibernate: mapeamento de entidades persistentes em pojos de domínio, resolvendo o ORM, abstraindo a escrita de código SQL (DDL e DML).
 - H2: Banco de Dados embutido, já com SQL iniciado
 - Bean Validations: framework para definição de regras de validação em entidades JPA via anotações.
 - Logback: geração de logs.
 - Spring Data JPA: tecnologia responsável por gerar boa parte do código relacionado a camada de persistência. No nível da aplicação eu escrevo os contratos de persistência, que funcionam como ponto de partida para a criação dos comandos de manipulação (CRUD), consultas simples e sofisticadas.
 - Spring Web MVC: framework web usado como solução MVC e para a definição de componentes seguindo o modelo arquitetural REST.
 - Jackson: API para conversão de dados Java em Json e vice-versa.
 - Postman

License
----
GNU PUBLIC LICENSE


----------


**Gustavo Carvalho**
